﻿namespace GörselEhliyetTakip2B
{
    partial class anaform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.txtTel = new System.Windows.Forms.TextBox();
            this.txtPostaKod = new System.Windows.Forms.TextBox();
            this.txtSemt = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.txtAd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAdayno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEposta = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCepTel = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoyad = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.txtTcNo = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.txtDin = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtSiraNo = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txtASiraNo = new System.Windows.Forms.TextBox();
            this.txtSeriNo = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txtCilt = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtBabaAdi = new System.Windows.Forms.TextBox();
            this.txtMahKöy = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtAnneAdi = new System.Windows.Forms.TextBox();
            this.txtİlce = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtDogumYeri = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.GrpYazılı = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.txtMotor = new System.Windows.Forms.TextBox();
            this.txtİlkYardm = new System.Windows.Forms.TextBox();
            this.txtTrafik = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.kn = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.lblSnvTrh = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.chTrafik = new System.Windows.Forms.CheckBox();
            this.chİlkYardım = new System.Windows.Forms.CheckBox();
            this.grpMuaf = new System.Windows.Forms.GroupBox();
            this.chMotor = new System.Windows.Forms.CheckBox();
            this.txtSinavSonuc = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblSnvZmn = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.GrpYazılı.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.grpMuaf.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox3.Location = new System.Drawing.Point(18, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(786, 81);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "İşlemler";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(333, 28);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Kaydı Sil";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(229, 27);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Kayıt Ara";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(124, 27);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Kaydet";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(21, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Yeni Kayıt";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(1, 100);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(938, 326);
            this.tabControl1.TabIndex = 26;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(930, 300);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Kişisel Bilgiler";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.txtFax);
            this.groupBox1.Controls.Add(this.txtTel);
            this.groupBox1.Controls.Add(this.txtPostaKod);
            this.groupBox1.Controls.Add(this.txtSemt);
            this.groupBox1.Controls.Add(this.richTextBox1);
            this.groupBox1.Controls.Add(this.label51);
            this.groupBox1.Controls.Add(this.label52);
            this.groupBox1.Controls.Add(this.label53);
            this.groupBox1.Controls.Add(this.label54);
            this.groupBox1.Controls.Add(this.label55);
            this.groupBox1.Controls.Add(this.label56);
            this.groupBox1.Controls.Add(this.txtAd);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtAdayno);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtEposta);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtCepTel);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtSoyad);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(462, 283);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kişisel Bilgiler";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(90, 227);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 23);
            this.textBox5.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Adı:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(90, 199);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 23);
            this.textBox4.TabIndex = 38;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(90, 254);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 23);
            this.textBox3.TabIndex = 37;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(275, 146);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 36;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(89, 120);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 35;
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(288, 226);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(100, 23);
            this.txtFax.TabIndex = 34;
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(275, 201);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(100, 23);
            this.txtTel.TabIndex = 33;
            // 
            // txtPostaKod
            // 
            this.txtPostaKod.Location = new System.Drawing.Point(275, 174);
            this.txtPostaKod.Name = "txtPostaKod";
            this.txtPostaKod.Size = new System.Drawing.Size(100, 23);
            this.txtPostaKod.TabIndex = 32;
            // 
            // txtSemt
            // 
            this.txtSemt.Location = new System.Drawing.Point(275, 117);
            this.txtSemt.Name = "txtSemt";
            this.txtSemt.Size = new System.Drawing.Size(100, 23);
            this.txtSemt.TabIndex = 30;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(275, 29);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(172, 79);
            this.richTextBox1.TabIndex = 29;
            this.richTextBox1.Text = "";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(240, 233);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(29, 15);
            this.label51.TabIndex = 27;
            this.label51.Text = "Fax:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(219, 205);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(51, 15);
            this.label52.TabIndex = 26;
            this.label52.Text = "Telefon:";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(197, 179);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(72, 15);
            this.label53.TabIndex = 25;
            this.label53.Text = "Posta Kodu:";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(227, 147);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(39, 15);
            this.label54.TabIndex = 24;
            this.label54.Text = "Şehir:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(228, 120);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(40, 15);
            this.label55.TabIndex = 23;
            this.label55.Text = "Semt:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(228, 32);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(46, 15);
            this.label56.TabIndex = 22;
            this.label56.Text = "Adresi:";
            // 
            // txtAd
            // 
            this.txtAd.Location = new System.Drawing.Point(89, 40);
            this.txtAd.Name = "txtAd";
            this.txtAd.Size = new System.Drawing.Size(100, 23);
            this.txtAd.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Soyadı:";
            // 
            // txtAdayno
            // 
            this.txtAdayno.Enabled = false;
            this.txtAdayno.Location = new System.Drawing.Point(89, 92);
            this.txtAdayno.Name = "txtAdayno";
            this.txtAdayno.Size = new System.Drawing.Size(100, 23);
            this.txtAdayno.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Aday No:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Doğum Tarihi:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Cep Telefonu:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "E-Posta:";
            // 
            // txtEposta
            // 
            this.txtEposta.Location = new System.Drawing.Point(90, 170);
            this.txtEposta.Name = "txtEposta";
            this.txtEposta.Size = new System.Drawing.Size(100, 23);
            this.txtEposta.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 199);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Cinsiyet:";
            // 
            // txtCepTel
            // 
            this.txtCepTel.Location = new System.Drawing.Point(89, 144);
            this.txtCepTel.Name = "txtCepTel";
            this.txtCepTel.Size = new System.Drawing.Size(100, 23);
            this.txtCepTel.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 226);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = " Ehliyet Sınıfı:";
            // 
            // txtSoyad
            // 
            this.txtSoyad.Location = new System.Drawing.Point(89, 66);
            this.txtSoyad.Name = "txtSoyad";
            this.txtSoyad.Size = new System.Drawing.Size(100, 23);
            this.txtSoyad.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 254);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Kayıt Tarihi:";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox2.Controls.Add(this.textBox9);
            this.groupBox2.Controls.Add(this.textBox8);
            this.groupBox2.Controls.Add(this.textBox7);
            this.groupBox2.Controls.Add(this.textBox6);
            this.groupBox2.Controls.Add(this.txtTcNo);
            this.groupBox2.Controls.Add(this.label45);
            this.groupBox2.Controls.Add(this.label44);
            this.groupBox2.Controls.Add(this.label46);
            this.groupBox2.Controls.Add(this.txtDin);
            this.groupBox2.Controls.Add(this.label43);
            this.groupBox2.Controls.Add(this.txtSiraNo);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this.txtASiraNo);
            this.groupBox2.Controls.Add(this.txtSeriNo);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.txtCilt);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.txtBabaAdi);
            this.groupBox2.Controls.Add(this.txtMahKöy);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.txtAnneAdi);
            this.groupBox2.Controls.Add(this.txtİlce);
            this.groupBox2.Controls.Add(this.label37);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.txtDogumYeri);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox2.Location = new System.Drawing.Point(474, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(468, 283);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Nüfus Bilgileri";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(326, 140);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 23);
            this.textBox9.TabIndex = 64;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(326, 165);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 23);
            this.textBox8.TabIndex = 63;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(93, 194);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(146, 23);
            this.textBox7.TabIndex = 62;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(93, 168);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(146, 23);
            this.textBox6.TabIndex = 61;
            // 
            // txtTcNo
            // 
            this.txtTcNo.Location = new System.Drawing.Point(94, 63);
            this.txtTcNo.Name = "txtTcNo";
            this.txtTcNo.Size = new System.Drawing.Size(146, 23);
            this.txtTcNo.TabIndex = 47;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(258, 143);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(68, 15);
            this.label45.TabIndex = 44;
            this.label45.Text = "Kan Grubu:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(294, 120);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(32, 15);
            this.label44.TabIndex = 43;
            this.label44.Text = "Dini:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(252, 170);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(76, 15);
            this.label46.TabIndex = 45;
            this.label46.Text = "Verildiği Yer:";
            // 
            // txtDin
            // 
            this.txtDin.Location = new System.Drawing.Point(326, 113);
            this.txtDin.Name = "txtDin";
            this.txtDin.Size = new System.Drawing.Size(100, 23);
            this.txtDin.TabIndex = 58;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(276, 92);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(50, 15);
            this.label43.TabIndex = 42;
            this.label43.Text = "Sıra No:";
            // 
            // txtSiraNo
            // 
            this.txtSiraNo.Location = new System.Drawing.Point(326, 87);
            this.txtSiraNo.Name = "txtSiraNo";
            this.txtSiraNo.Size = new System.Drawing.Size(100, 23);
            this.txtSiraNo.TabIndex = 57;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(252, 64);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(74, 15);
            this.label42.TabIndex = 41;
            this.label42.Text = "Aile Sıra No:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(38, 42);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(50, 15);
            this.label32.TabIndex = 31;
            this.label32.Text = "Seri-no:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(297, 36);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(29, 15);
            this.label41.TabIndex = 40;
            this.label41.Text = "Cilt:";
            // 
            // txtASiraNo
            // 
            this.txtASiraNo.Location = new System.Drawing.Point(326, 61);
            this.txtASiraNo.Name = "txtASiraNo";
            this.txtASiraNo.Size = new System.Drawing.Size(100, 23);
            this.txtASiraNo.TabIndex = 56;
            // 
            // txtSeriNo
            // 
            this.txtSeriNo.Location = new System.Drawing.Point(94, 36);
            this.txtSeriNo.Name = "txtSeriNo";
            this.txtSeriNo.Size = new System.Drawing.Size(146, 23);
            this.txtSeriNo.TabIndex = 46;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(7, 70);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(81, 15);
            this.label33.TabIndex = 32;
            this.label33.Text = "T.C Kimlik No:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(11, 249);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(80, 15);
            this.label40.TabIndex = 39;
            this.label40.Text = "Mahalle/Köy:";
            // 
            // txtCilt
            // 
            this.txtCilt.Location = new System.Drawing.Point(326, 35);
            this.txtCilt.Name = "txtCilt";
            this.txtCilt.Size = new System.Drawing.Size(100, 23);
            this.txtCilt.TabIndex = 55;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(62, 223);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(29, 15);
            this.label39.TabIndex = 38;
            this.label39.Text = "İlçe:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(28, 95);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(61, 15);
            this.label34.TabIndex = 33;
            this.label34.Text = " Baba Adı:";
            // 
            // txtBabaAdi
            // 
            this.txtBabaAdi.Location = new System.Drawing.Point(94, 88);
            this.txtBabaAdi.Name = "txtBabaAdi";
            this.txtBabaAdi.Size = new System.Drawing.Size(146, 23);
            this.txtBabaAdi.TabIndex = 48;
            // 
            // txtMahKöy
            // 
            this.txtMahKöy.Location = new System.Drawing.Point(93, 246);
            this.txtMahKöy.Name = "txtMahKöy";
            this.txtMahKöy.Size = new System.Drawing.Size(148, 23);
            this.txtMahKöy.TabIndex = 54;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(71, 196);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(20, 15);
            this.label38.TabIndex = 37;
            this.label38.Text = "İli:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(28, 120);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(61, 15);
            this.label35.TabIndex = 34;
            this.label35.Text = "Anne Adı:";
            // 
            // txtAnneAdi
            // 
            this.txtAnneAdi.Location = new System.Drawing.Point(94, 114);
            this.txtAnneAdi.Name = "txtAnneAdi";
            this.txtAnneAdi.Size = new System.Drawing.Size(146, 23);
            this.txtAnneAdi.TabIndex = 49;
            // 
            // txtİlce
            // 
            this.txtİlce.Location = new System.Drawing.Point(93, 220);
            this.txtİlce.Name = "txtİlce";
            this.txtİlce.Size = new System.Drawing.Size(148, 23);
            this.txtİlce.TabIndex = 53;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(15, 170);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(76, 15);
            this.label37.TabIndex = 36;
            this.label37.Text = "Medeni Hali:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(15, 147);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(74, 15);
            this.label36.TabIndex = 35;
            this.label36.Text = "Doğum Yeri:";
            // 
            // txtDogumYeri
            // 
            this.txtDogumYeri.Location = new System.Drawing.Point(94, 140);
            this.txtDogumYeri.Name = "txtDogumYeri";
            this.txtDogumYeri.Size = new System.Drawing.Size(146, 23);
            this.txtDogumYeri.TabIndex = 50;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(930, 300);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Kurs Bilgileri";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Controls.Add(this.grpMuaf);
            this.tabPage3.Controls.Add(this.GrpYazılı);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(930, 300);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Sınavlar";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(930, 300);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Ödemeler";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // GrpYazılı
            // 
            this.GrpYazılı.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.GrpYazılı.Controls.Add(this.textBox16);
            this.GrpYazılı.Controls.Add(this.label28);
            this.GrpYazılı.Controls.Add(this.btn3);
            this.GrpYazılı.Controls.Add(this.btn2);
            this.GrpYazılı.Controls.Add(this.btn1);
            this.GrpYazılı.Controls.Add(this.txtMotor);
            this.GrpYazılı.Controls.Add(this.txtİlkYardm);
            this.GrpYazılı.Controls.Add(this.txtTrafik);
            this.GrpYazılı.Controls.Add(this.label11);
            this.GrpYazılı.Controls.Add(this.kn);
            this.GrpYazılı.Controls.Add(this.lbl);
            this.GrpYazılı.Controls.Add(this.lblSnvTrh);
            this.GrpYazılı.Location = new System.Drawing.Point(32, 6);
            this.GrpYazılı.Name = "GrpYazılı";
            this.GrpYazılı.Size = new System.Drawing.Size(290, 236);
            this.GrpYazılı.TabIndex = 27;
            this.GrpYazılı.TabStop = false;
            this.GrpYazılı.Text = "Yazılı Sınavlar";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(95, 76);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(51, 13);
            this.label28.TabIndex = 14;
            this.label28.Text = "NOTLAR";
            this.label28.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(189, 153);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(75, 22);
            this.btn3.TabIndex = 12;
            this.btn3.Text = "girmedi";
            this.btn3.UseVisualStyleBackColor = true;
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(189, 124);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(75, 22);
            this.btn2.TabIndex = 11;
            this.btn2.Text = "girmedi";
            this.btn2.UseVisualStyleBackColor = true;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(189, 96);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(75, 23);
            this.btn1.TabIndex = 10;
            this.btn1.Text = "girmedi";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // txtMotor
            // 
            this.txtMotor.Location = new System.Drawing.Point(70, 150);
            this.txtMotor.Name = "txtMotor";
            this.txtMotor.Size = new System.Drawing.Size(100, 20);
            this.txtMotor.TabIndex = 16;
            // 
            // txtİlkYardm
            // 
            this.txtİlkYardm.Location = new System.Drawing.Point(70, 124);
            this.txtİlkYardm.Name = "txtİlkYardm";
            this.txtİlkYardm.Size = new System.Drawing.Size(100, 20);
            this.txtİlkYardm.TabIndex = 15;
            // 
            // txtTrafik
            // 
            this.txtTrafik.Location = new System.Drawing.Point(70, 98);
            this.txtTrafik.Name = "txtTrafik";
            this.txtTrafik.Size = new System.Drawing.Size(100, 20);
            this.txtTrafik.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 153);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Motor";
            // 
            // kn
            // 
            this.kn.AutoSize = true;
            this.kn.Location = new System.Drawing.Point(3, 127);
            this.kn.Name = "kn";
            this.kn.Size = new System.Drawing.Size(50, 13);
            this.kn.TabIndex = 4;
            this.kn.Text = "İlkYardım";
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(19, 101);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(34, 13);
            this.lbl.TabIndex = 3;
            this.lbl.Text = "Trafik";
            // 
            // lblSnvTrh
            // 
            this.lblSnvTrh.AutoSize = true;
            this.lblSnvTrh.Location = new System.Drawing.Point(3, 43);
            this.lblSnvTrh.Name = "lblSnvTrh";
            this.lblSnvTrh.Size = new System.Drawing.Size(66, 13);
            this.lblSnvTrh.TabIndex = 2;
            this.lblSnvTrh.Text = "Sınav Tarihi:";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(70, 43);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(100, 20);
            this.textBox16.TabIndex = 17;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox4.Controls.Add(this.textBox15);
            this.groupBox4.Controls.Add(this.textBox14);
            this.groupBox4.Controls.Add(this.textBox13);
            this.groupBox4.Controls.Add(this.textBox12);
            this.groupBox4.Controls.Add(this.textBox11);
            this.groupBox4.Controls.Add(this.textBox10);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Location = new System.Drawing.Point(13, 36);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(509, 184);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Kurs Bilgileri";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(125, 45);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(121, 22);
            this.textBox15.TabIndex = 48;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(125, 87);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(121, 22);
            this.textBox14.TabIndex = 47;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(125, 127);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(121, 22);
            this.textBox13.TabIndex = 46;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(362, 36);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(121, 22);
            this.textBox12.TabIndex = 45;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(361, 125);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(121, 22);
            this.textBox11.TabIndex = 44;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(362, 83);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(121, 22);
            this.textBox10.TabIndex = 43;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.Location = new System.Drawing.Point(313, 127);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 15);
            this.label10.TabIndex = 42;
            this.label10.Text = "Sayısı:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label31.Location = new System.Drawing.Point(313, 87);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(37, 15);
            this.label31.TabIndex = 41;
            this.label31.Text = "Tarih:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label30.Location = new System.Drawing.Point(286, 41);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(64, 15);
            this.label30.TabIndex = 40;
            this.label30.Text = "Verildiği İl:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label26.Location = new System.Drawing.Point(16, 127);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(85, 15);
            this.label26.TabIndex = 39;
            this.label26.Text = "Sertifika Sınıfı:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label25.Location = new System.Drawing.Point(31, 87);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(70, 15);
            this.label25.TabIndex = 38;
            this.label25.Text = "Kayıt Tarihi:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label24.Location = new System.Drawing.Point(47, 45);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 15);
            this.label24.TabIndex = 37;
            this.label24.Text = "Dönemi:";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(22, 153);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 6;
            this.button6.Text = "Sıfırla";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // chTrafik
            // 
            this.chTrafik.AutoSize = true;
            this.chTrafik.Location = new System.Drawing.Point(25, 71);
            this.chTrafik.Name = "chTrafik";
            this.chTrafik.Size = new System.Drawing.Size(53, 17);
            this.chTrafik.TabIndex = 4;
            this.chTrafik.Text = "Trafik";
            this.chTrafik.UseVisualStyleBackColor = true;
            // 
            // chİlkYardım
            // 
            this.chİlkYardım.AutoSize = true;
            this.chİlkYardım.Location = new System.Drawing.Point(25, 44);
            this.chİlkYardım.Name = "chİlkYardım";
            this.chİlkYardım.Size = new System.Drawing.Size(72, 17);
            this.chİlkYardım.TabIndex = 3;
            this.chİlkYardım.Text = "İlk Yardım";
            this.chİlkYardım.UseVisualStyleBackColor = true;
            // 
            // grpMuaf
            // 
            this.grpMuaf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.grpMuaf.Controls.Add(this.button6);
            this.grpMuaf.Controls.Add(this.chMotor);
            this.grpMuaf.Controls.Add(this.chTrafik);
            this.grpMuaf.Controls.Add(this.chİlkYardım);
            this.grpMuaf.Location = new System.Drawing.Point(360, 6);
            this.grpMuaf.Name = "grpMuaf";
            this.grpMuaf.Size = new System.Drawing.Size(133, 236);
            this.grpMuaf.TabIndex = 28;
            this.grpMuaf.TabStop = false;
            this.grpMuaf.Text = "Muaf Olunan Sınavlar";
            // 
            // chMotor
            // 
            this.chMotor.AutoSize = true;
            this.chMotor.Location = new System.Drawing.Point(25, 98);
            this.chMotor.Name = "chMotor";
            this.chMotor.Size = new System.Drawing.Size(53, 17);
            this.chMotor.TabIndex = 5;
            this.chMotor.Text = "Motor";
            this.chMotor.UseVisualStyleBackColor = true;
            // 
            // txtSinavSonuc
            // 
            this.txtSinavSonuc.Location = new System.Drawing.Point(109, 156);
            this.txtSinavSonuc.Name = "txtSinavSonuc";
            this.txtSinavSonuc.Size = new System.Drawing.Size(104, 20);
            this.txtSinavSonuc.TabIndex = 21;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(26, 161);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(77, 13);
            this.label27.TabIndex = 16;
            this.label27.Text = "Sınav Sonucu:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 64);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Kullanılan Araç:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(66, 100);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 13);
            this.label15.TabIndex = 7;
            this.label15.Text = "Plaka:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(43, 128);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Güzergah:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(37, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Sınav Tarihi:";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox5.Controls.Add(this.textBox20);
            this.groupBox5.Controls.Add(this.textBox19);
            this.groupBox5.Controls.Add(this.textBox18);
            this.groupBox5.Controls.Add(this.textBox17);
            this.groupBox5.Controls.Add(this.txtSinavSonuc);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.lblSnvZmn);
            this.groupBox5.Location = new System.Drawing.Point(536, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(362, 236);
            this.groupBox5.TabIndex = 29;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "DireksiyonSınavları";
            // 
            // lblSnvZmn
            // 
            this.lblSnvZmn.AutoSize = true;
            this.lblSnvZmn.Location = new System.Drawing.Point(37, 35);
            this.lblSnvZmn.Name = "lblSnvZmn";
            this.lblSnvZmn.Size = new System.Drawing.Size(0, 13);
            this.lblSnvZmn.TabIndex = 0;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(109, 121);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(104, 20);
            this.textBox17.TabIndex = 22;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(109, 93);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(104, 20);
            this.textBox18.TabIndex = 23;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(109, 57);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(104, 20);
            this.textBox19.TabIndex = 24;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(109, 28);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(104, 20);
            this.textBox20.TabIndex = 25;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(434, 28);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "Çıkış";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // anaform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 425);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox3);
            this.Name = "anaform";
            this.Text = "anaform";
            this.groupBox3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.GrpYazılı.ResumeLayout(false);
            this.GrpYazılı.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.grpMuaf.ResumeLayout(false);
            this.grpMuaf.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtFax;
        private System.Windows.Forms.TextBox txtTel;
        private System.Windows.Forms.TextBox txtPostaKod;
        private System.Windows.Forms.TextBox txtSemt;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox txtAd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAdayno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEposta;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCepTel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSoyad;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox txtTcNo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtDin;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtSiraNo;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtASiraNo;
        private System.Windows.Forms.TextBox txtSeriNo;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtCilt;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtBabaAdi;
        private System.Windows.Forms.TextBox txtMahKöy;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtAnneAdi;
        private System.Windows.Forms.TextBox txtİlce;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtDogumYeri;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox GrpYazılı;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.TextBox txtMotor;
        private System.Windows.Forms.TextBox txtİlkYardm;
        private System.Windows.Forms.TextBox txtTrafik;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label kn;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.Label lblSnvTrh;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox grpMuaf;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.CheckBox chMotor;
        private System.Windows.Forms.CheckBox chTrafik;
        private System.Windows.Forms.CheckBox chİlkYardım;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox txtSinavSonuc;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblSnvZmn;
    }
}